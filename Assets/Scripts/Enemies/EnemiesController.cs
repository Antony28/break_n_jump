﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesController : MonoBehaviour
{
    public string status = "patrolling";
    public int initialHealth;
    public int currentHealth;
    public bool goingToExplode = false;

    void Start()
    {
        status = "patrolling";
        if (gameObject.name == "Chiquito")
        {
            initialHealth = 2;
        }

        if (gameObject.name == "Fijador")
        {
            initialHealth = 2;
        }
        if (gameObject.name == "Normalito")
        {
            initialHealth = 6;
        }
        currentHealth = initialHealth;
    }

    public void detectPlayer()
    {
        status = "following";
    }
}
