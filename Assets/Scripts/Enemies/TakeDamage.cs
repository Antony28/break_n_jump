﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamage : MonoBehaviour
{
    public EnemiesController enemiesController;

    public GameObject enemyGO;

    public TakeDamageAnim takeDamageAnim;

    void Update()
    {
        Destroy();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerHitBox"))
        {
            takeDamage();
            enemiesController.detectPlayer();
            Destroy();
        }
    }

    void takeDamage()
    {
        enemiesController.currentHealth -= 2;
        takeDamageAnim.changeColor();
        StartCoroutine(wait());
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.1f);
        takeDamageAnim.changeNormalColor();
    }

    void Destroy()
    {
        if (enemiesController.currentHealth <= 0)
        {
            Destroy(enemyGO);
            Score.score++;
        }
    }
}
