﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPlayer : MonoBehaviour
{
    public Rigidbody playerRigidbody;
    public NavMeshAgent enemyAgent;
    public EnemiesController enemyController;

    void Update()
    {
        checkForStatus();
    }

    void checkForStatus()
    {
        if (enemyController.status == "following")
        {
            followPlayer();
        }
    }

    public void followPlayer()
    {
        enemyAgent.SetDestination(playerRigidbody.position);

    }
}
