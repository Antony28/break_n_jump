﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPlayer : MonoBehaviour
{
    PlayerVista lifeComp;
    public bool AttackOn;

    float initialTime = 0;
    public float timeToAttack;

    void Start()
    {
        lifeComp = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerVista>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            AttackOn = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            AttackOn = false;
        }
    }
    public void AttackPlayer()
    {
        if (AttackOn)
        {
            initialTime += Time.deltaTime;
            if (initialTime >= timeToAttack)
            {
                lifeComp.TakeDamage(2);
                lifeComp.SetLife();
                initialTime = 0;
            }
        }
        else
        {
            initialTime = 0;
        }
    }
}
