﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBProjectile : MonoBehaviour
{

    float speed = 20;
    public Rigidbody projectileRigidbody;

    PlayerVista playerVista;

    void Start()
    {
        playerVista = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerVista>();
        projectileRigidbody.velocity = transform.forward * speed;
        StartCoroutine(destroyProjectile());
    }

    IEnumerator destroyProjectile()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            playerVista.TakeDamage(2);
        }
        if(other.gameObject.name != "BossBattle" && other.gameObject.name != "HeadHitBox")
        {
            Destroy(gameObject);
        }
    }
}
