﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBShooting : MonoBehaviour
{

    public GameObject projectilePrefab;
    public float rechargeTime;
    public bool isCharged = true;

    public GameObject player;


    public EnemiesController enemyController;

    void Start()
    {
        
    }

    void Update()
    {
        checkForStatus();
        lookAtPlayer();
    }

    void checkForStatus()
    {
        if (enemyController.status == "following" && isCharged == true)
        {
            Fire();
        }
    }

    void lookAtPlayer()
    {
        transform.LookAt(player.transform.position);
    }

    void Fire()
    {
        Instantiate(projectilePrefab, transform.position, transform.rotation);
        isCharged = false;
        StartCoroutine(Recharge());
    }

    IEnumerator Recharge()
    {
        yield return new WaitForSeconds(rechargeTime);
        isCharged = true;
    }
}
