﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patrolling : MonoBehaviour
{
    public Rigidbody point1;
    public GameObject point1GO;
    public Rigidbody point2;
    public GameObject point2GO;
    public Rigidbody point3;
    public GameObject point3GO;
    public Rigidbody point4;
    public GameObject point4GO;

    public Rigidbody nextPoint;
    public GameObject nextPointGO;

    public EnemiesController enemiesController;

    public NavMeshAgent enemyAgent;


    void Start()
    {
        nextPoint = point1;
        nextPointGO = point1GO;
        goToNextPoint();
    }


    void goToNextPoint()
    {        
            enemyAgent.SetDestination(nextPoint.position);       
    }

    void OnTriggerEnter(Collider other)
    {
        if (enemiesController.status == "patrolling" && other.gameObject == nextPointGO)
        {
            setNextPoint();
            goToNextPoint();
        }
        
    }

    void setNextPoint()
    {
        if (nextPoint==point1)
        {
            nextPoint = point2;
            nextPointGO = point2GO;
        }
        else if (nextPoint == point2)
        {
            nextPoint = point3;
            nextPointGO = point3GO;
        }
        else if (nextPoint == point3)
        {
            nextPoint = point4;
            nextPointGO = point4GO;
        }
        else if (nextPoint == point4)
        {
            nextPoint = point1;
            nextPointGO = point1GO;
        }
    }

}
