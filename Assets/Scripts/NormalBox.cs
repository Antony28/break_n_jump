﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBox : MonoBehaviour
{
    public bool dropItem;
    public GameObject itemDropped;
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PlayerHitBox"))
        {
            if(dropItem)
            {
                Instantiate(itemDropped, transform.position, transform.rotation);
            }
            Destroy(gameObject);
            Score.score++;
        }
    }
}
