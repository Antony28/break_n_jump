﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallenEnemyScript : MonoBehaviour
{
    public GameObject fallenEnemy;
    public Transform fallenEnemySpawner;
    public bool used;

    private void Start()
    {
        used = false;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && used == false)
        {
            Instantiate(fallenEnemy, fallenEnemySpawner.position, transform.rotation);
            used = true;
        }
    }
}
