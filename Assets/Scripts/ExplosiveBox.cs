﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBox : MonoBehaviour
{
    public GameObject explosionCol;
    public PreExplosionAnim preExplosionAnim;

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            preExplosionAnim.startAnim();
            StartCoroutine(wait());
        }
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(1.5f);
        Explode();
    }

    void Explode()
    {
        explosionCol.SetActive(true);
        StartCoroutine(destroyObject());
    }

    IEnumerator destroyObject()
    {
        yield return new WaitForSeconds(0.1f);
        Destroy(this.gameObject);
        Score.score++;
    }
}
