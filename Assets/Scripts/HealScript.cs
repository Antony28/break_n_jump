﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealScript : MonoBehaviour
{
    PlayerModelo playerModelo;
    void Start()
    {
        playerModelo = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerModelo>();
    }
    void Update()
    {
        transform.Rotate( 0, 90 * Time.deltaTime, 0);
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            playerModelo.currentLife = playerModelo.initialLife;
            Destroy(gameObject);
        }
    }
}
