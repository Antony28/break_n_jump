﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionDmg : MonoBehaviour
{
    PlayerVista lifeComp;

    private void Start()
    {
        lifeComp = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerVista>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            lifeComp.TakeDamage(2);
            lifeComp.SetLife();
        }
    }
}
