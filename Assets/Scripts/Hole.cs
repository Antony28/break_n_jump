﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Hole : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "HeadPlayer")
        {
            SceneManager.LoadScene("Lose");
        }
        if(other.gameObject.name == "Fallen Enemy")
        {
            Destroy(other.gameObject);
        }
    }
}
