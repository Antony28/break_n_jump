﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    Door doorComp;
    public GameObject door;

    void Start()
    {
        doorComp = door.GetComponent<Door>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "PlayerHitBox")
        {
            doorComp.SlideDoor(true);
        }
}
}
