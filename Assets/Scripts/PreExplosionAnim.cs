﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreExplosionAnim : MonoBehaviour
{
    Color red;
    Color white;

    void Start()
    {
        red = GetComponent<Renderer>().material.color;
        red.r = 255;
        red.g = 0;
        red.b = 0;

        white = GetComponent<Renderer>().material.color;


        GetComponent<Renderer>().material.color = white;
    }

    void changeColor()
    {
        if (GetComponent<Renderer>().material.color == white)
        {
            GetComponent<Renderer>().material.color = red;
        }
        else if (GetComponent<Renderer>().material.color == red)
        {
            GetComponent<Renderer>().material.color = white;
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.1f);
        changeColor();
        StartCoroutine(Wait());
    }

    public void startAnim()
    {
        StartCoroutine(Wait());
    }
}

