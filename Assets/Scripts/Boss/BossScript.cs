﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour
{
    public GameObject shield;
    public GameObject láserObj;
    public GameObject gun;
    public GunScript gunScript;
    public Rigidbody playerRB;
    public TakeDamageAnim takeDamageAnim;

    public bool fijarObjetivo;
    public bool vulnerable;
    public bool rotarActivo;

    public int initialLife;
    public int currentLife;

    public float tiempoParaComenzar;
    public float duraciónDeFaseDeDisparo;
    float velocidadDeRotado;
    public float tiempoDeRotado;
    public float duraciónDeVulnerabilidad;
    public int nGiros;

    void Start()
    {
        shield.SetActive(false);
        currentLife = initialLife;
        gun.SetActive(false);
        láserObj.SetActive(false);
        fijarObjetivo = false;
        vulnerable = false;
    }

    void Update()
    {
        if (fijarObjetivo == true)
        {
            LookPlayer();
        }
        Die();

        if(rotarActivo)
        {
            velocidadDeRotado = 360 / tiempoDeRotado;
            RotarEnEjeY();
        }
    }

    // Mirar hacia el player.
    void LookPlayer()
    {
        Vector3 direction = (playerRB.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = lookRotation;
    }
    void RotarEnEjeY()
    {
        transform.Rotate(0, velocidadDeRotado * Time.deltaTime, 0);

    }

    // Comienza la batalla.
    public void StartBattle()
    {
        Debug.Log("¡Comienza la batalla!");
        // Comienza Fase de Disparo.
        StartCoroutine(Shootear()); 
    }

    // Fase de Disparo.
    IEnumerator Shootear()
    {
        Debug.Log("¡Fase de disparos!");
        fijarObjetivo = true;
        yield return new WaitForSeconds(tiempoParaComenzar);
        shield.SetActive(true);
        gunScript.isCharged = true;
        gun.SetActive(true);
        yield return new WaitForSeconds(duraciónDeFaseDeDisparo);
        // Comienza Fase de Láser.
        StartCoroutine(ElRashoLáser());
    }

    // Fase de Láser.
    IEnumerator ElRashoLáser()
    {
        gun.SetActive(false);
        Debug.Log("¡Fase de láser!");
        fijarObjetivo = false;
        yield return new WaitForSeconds(tiempoParaComenzar);

        láserObj.SetActive(true);
        rotarActivo = true;
        yield return new WaitForSeconds(tiempoDeRotado * nGiros);
        rotarActivo = false;
        láserObj.SetActive(false);
        // Comienza Fase de Vulnerabilidad.
        StartCoroutine(ShieldOff());
    }

    // Fase de Vulnerabilidad.
    IEnumerator EsVulnerable()
    {
        Debug.Log("¡Fase de contraataque!");
        vulnerable = true;
        yield return new WaitForSeconds(duraciónDeVulnerabilidad);
        vulnerable = false;
        shield.SetActive(true);
        // Comienza Fase de Disparo.
        StartCoroutine(Shootear());
    }
    // Recibir daño en Fase de Vulnerabilidad.
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PlayerHitBox") && vulnerable == true)
        {
            currentLife--;
            takeDamageAnim.changeColor();
            StartCoroutine(wait());
            vulnerable = false;
        }
    }
    // Morir.
    void Die()
    {
        if (currentLife <= 0)
        {
            Destroy(gameObject);
        }
    }
    //Feedback
    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.1f);
        takeDamageAnim.changeNormalColor();
    }
    IEnumerator ShieldOff()
    {
        yield return new WaitForSeconds(0.1f);
        shield.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        shield.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        shield.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        shield.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        shield.SetActive(false);
        StartCoroutine(EsVulnerable());
    }
}