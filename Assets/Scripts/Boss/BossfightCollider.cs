﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossfightCollider : MonoBehaviour
{
    public BossScript boss;
    public bool activeBoss;
    public float waitVictory;

    void Start()
    {
        activeBoss = false;
    }

    void Update()
    {
        if(boss.currentLife == 0)
        {
            StartCoroutine(YouWin());
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && activeBoss == false)    
        {
            activeBoss = true;
            boss.StartBattle();
        }
    }
    IEnumerator YouWin()
    {
        yield return new WaitForSeconds(waitVictory);
        SceneManager.LoadScene("Win");
    }
}
