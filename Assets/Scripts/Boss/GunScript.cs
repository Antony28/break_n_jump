﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour
{
    public Transform bossTransform;
    public GameObject projectilePrefab;
    public float cdDeDisparo;
    public bool isCharged;

    void Start()
    {
        isCharged = true;
    }
    void Update()
    {
        Shoot();
    }
    void Shoot()
    {
        if (isCharged)
        {
            Fire();
        }
    }
    void Fire()
    {
        Instantiate(projectilePrefab, transform.position, bossTransform.rotation);
        isCharged = false;
        StartCoroutine(Recharge());
    }
    IEnumerator Recharge()
    {
        yield return new WaitForSeconds(cdDeDisparo);
        isCharged = true;
    }
}
