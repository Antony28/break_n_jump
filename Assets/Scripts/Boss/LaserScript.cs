﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    PlayerVista playerVista;
    public int damage;
    public bool playerDamaged;

    private void Start()
    {
        playerVista = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerVista>();
        playerDamaged = false;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && playerDamaged == false)
        {
            playerVista.TakeDamage(damage);
            playerDamaged = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerDamaged = false;
        }
    }
}
