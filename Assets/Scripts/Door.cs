﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{

    public GameObject door;

    Animator doorAnim;

    void Start()
    {
        doorAnim = door.GetComponent<Animator>();
    }

    public void SlideDoor(bool state)
    {
        doorAnim.SetBool("slide", state);
    }
}
