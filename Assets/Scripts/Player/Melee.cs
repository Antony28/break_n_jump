﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : MonoBehaviour
{
    public GameObject hitbox;
    public bool isAttacking=false;

    void Update()
    {
        startAttack();
    }

    void startAttack()
    {
        if (Input.GetKeyDown(KeyCode.X)&&isAttacking==false)
        {
            isAttacking = true;
            StartCoroutine(inputLag());
        }
    }

    IEnumerator inputLag()
    {
        yield return new WaitForSeconds(0.1f);
        Slash();

    }

    void Slash()
    {
        hitbox.SetActive(true);
        StartCoroutine(hitboxDuration());
    }

    IEnumerator hitboxDuration()
    {
        yield return new WaitForSeconds(0.2f);
        disableHitbox();
    }

    void disableHitbox()
    {
        hitbox.SetActive(false);
        StartCoroutine(endLag());
    }

    IEnumerator endLag()
    {
        yield return new WaitForSeconds(0.2f);
        isAttacking = false;
    }
}
