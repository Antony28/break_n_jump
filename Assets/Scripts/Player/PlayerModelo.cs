﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class PlayerModelo : MonoBehaviour
{
    //Life
    public float initialLife;
    public float currentLife;

    public Text lifeText;
    public Image healthBar;

    //Player
    public float moveSpeed;
    public float jumpForce;

    public CharacterController controller;

    public float gravityScale;

    //MVC

    public PlayerVista vista;

    void Start()
    {
        currentLife = initialLife;
        vista.SetLife();
    }
}
