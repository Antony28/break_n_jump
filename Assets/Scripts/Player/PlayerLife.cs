﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{

    public int maxHealth = 100;
    public int currentHealth;

    void Start()
    {
        currentHealth = maxHealth;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.G))
        {
            ReceiveDmg(20);
        }
        if (currentHealth <= 0)
        {
            SceneManager.LoadScene("Lose");
        }
    }

    public void ReceiveDmg(int damage)
    {
        currentHealth -= damage;
    }
}
