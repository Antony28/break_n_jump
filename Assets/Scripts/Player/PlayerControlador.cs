﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControlador : MonoBehaviour
{
    public PlayerModelo model;

    public Vector3 moveDirection;

    void Start()
    {
        model.controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        moveDirection = new Vector3(Input.GetAxis("Horizontal") * model.moveSpeed, moveDirection.y, Input.GetAxis("Vertical") * model.moveSpeed);

        if (model.controller.isGrounded)
        {
            moveDirection.y = 0;

            if (Input.GetButtonDown("Jump"))
            {
                moveDirection.y = model.jumpForce;
            }
        }
        moveDirection.y = moveDirection.y + (Physics.gravity.y * model.gravityScale * Time.deltaTime);
        model.controller.Move(moveDirection * Time.deltaTime);

       //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(moveDirection), 0.15F);
    }
}
