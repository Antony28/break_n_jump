﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerVista : MonoBehaviour
{
    public PlayerModelo model;

    public TakeDamageAnim takeDamageAnim;

    void Update()
    {
        SetLife();
    }
    public void SetLife()
    {
        model.lifeText.text = "Vidas: " + model.currentLife;
        model.healthBar.fillAmount = model.currentLife / model.initialLife;

        if (model.currentLife <= 0)
        {
            Die();
        }
        if(model.currentLife >= model.initialLife)
        {
            model.currentLife = model.initialLife;
        }
    }
    public void Die()
    {
        if (model.currentLife <= 0)
        {
            SceneManager.LoadScene("Lose");
        }
    }
    public void TakeDamage(int damage)
    {
        model.currentLife -= damage;
        takeDamageAnim.changeColor();
        StartCoroutine(wait());
        SetLife();
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.1f);
        takeDamageAnim.changeNormalColor();
    }
}
