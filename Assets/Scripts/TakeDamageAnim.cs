﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageAnim : MonoBehaviour
{
    Color red;
    Color white;

    void Start()
    {
        red = GetComponent<Renderer>().material.color;
        red.r = 255;
        red.g = 0;
        red.b = 0;

        white = GetComponent<Renderer>().material.color;


        GetComponent<Renderer>().material.color = white;
    }

    public void changeColor()
    {
        if (GetComponent<Renderer>().material.color == white)
        {
            GetComponent<Renderer>().material.color = red;
        }
    }
    public void changeNormalColor()
    {
        if (GetComponent<Renderer>().material.color == red)
        {
            GetComponent<Renderer>().material.color = white;
        }
    }
}
