﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    LevelLoader lvlLoader;
    void Start()
    {
        lvlLoader = GameObject.Find("LevelLoader").GetComponent<LevelLoader>();
    }
    public void PlayGame()
    {
        SceneManager.LoadScene("Nivel 01");
        //StartCoroutine(lvlLoader.LoadLevel(1));
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
