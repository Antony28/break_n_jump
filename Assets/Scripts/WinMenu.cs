﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinMenu : MonoBehaviour
{
    public void PlayAgain()
    {
        Score.score = 0;
        SceneManager.LoadScene("Nivel 01");
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
